# gpg-email-helper

This is just a little script to automatically find and import the public key used to sign an email. It will first check if the email itself contains the public key, and if it does not, then it will attempting to get the key from many keyservers.

## Usage

Just install the dependencies, then run it with the email file, like this:

```sh
python3 ./gpg-email-helper.py ~/Mail/inbox/10
```

You can view the log at `$XDG_DATA_HOME/gpg-email-helper/log` or `~/.local/share/gpg-email-helper/log`.

## Claws Mail

I use this Claws Mail filtering rule, it runs grep first to check if the email contains a signature, which helps speed it up when you have to run it in bulk. Just add it to `~/.claws-mail/matcherrc` under `[filtering]`

```text
enabled rulename "Run gpg-email-helper" test "grep -- \"-----BEGIN PGP SIGNATURE-----\" %F" execute "python3 {SCRIPT LOCATION}/gpg-email-helper.py %F"
```

Or to add it in the GUI:

1. Go to Configuration -> Filtering
2. Add a title (such as "Run gpg-email-helper")
3. Enter `test "grep -- \"-----BEGIN PGP SIGNATURE-----\" %F"` for the condition.
4. Enter `execute "python3 {SCRIPT LOCATION}/gpg-email-helper.py %F"` for the action.
5. Click "Add"
6. Click "OK"

### Processing (old) emails in bulk

With a filtering rule, Claws Mail will only run gpg-email-helper when it receives a new email (that contains a signature). To run it on all emails instead, you can add the rule to pre/post-processing instead, but can take upwards of a minute to load every time you open your inbox, can cause the POP3/IMAP connection to time out, and often just crashes Claws Mail. Instead, I'd just just running it manually if you want to import public keys from all your old emails.

Run this to run gpg-email-helper on all files with a signature:

```sh
bash -c 'for item in $(ls $HOME/Mail/inbox/); do if [ $(grep -c -- "-----BEGIN PGP SIGNATURE-----" $HOME/Mail/inbox/$item) != 
0 ]; then python3 ~/gpg-email-helper/gpg-email-helper.py $HOME/Mail/inbox/$item; fi; done'
```

## Todo

- Add async to get the key from all keyservers simultaneously
